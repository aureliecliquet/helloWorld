angular.module('starter.services', [])

.factory('VeillesFactory', function($http){
  return {
    getAll:function(){
    return $http.get('http://veille.popschool.fr/api/?api=veille&action=getAll');
    },
    get:function(veilleId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=get&id='+veilleId);
    }
  }
})

.factory('UsersFactory', function($http){
  return {
    getAll:function(){
    return $http.get('http://veille.popschool.fr/api/?api=user&action=getAll');
    },
    get:function(userId){
      return $http.get('http://veille.popschool.fr/api/?api=user&action=get&id='+userId);
    },
    getByUser:function(userId){
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getByUser&id_user='+userId);
    },
    auth:function(email, password) {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=auth&email=' + email + '&password=' + password);
    }
  }
})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Rémy Blancke',
    lastText: 'I\'m the Godgather?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Jérémy Bustin',
    lastText: 'Hey, it\'s the GodFather',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'David Demonchy',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Philippe Pary',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Thomas Hocedez',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});

